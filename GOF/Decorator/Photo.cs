﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace PhotoLibrary
{
    public interface IThumbnail
    {
        int Width { get; set; }
        int Height { get; set; }
        
        void Draw(Graphics gc, int x, int y);
    }

    // oryginalna klasa ThumbnailComponent
    public class ThumbnailComponent : IThumbnail
    {
        public Image ThumbnailImage { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public ThumbnailComponent()
        {
        }

        public ThumbnailComponent(string path, int width, int height, bool aspect)
        {
            Image image = new Bitmap(path);
            if (image.Height < image.Width)
            {
                Width = width;
                Height = aspect ? (int)(((double)image.Height / image.Width) * width) : height;
            }
            else
            {
                Height = height;
                Width = aspect ? (int)(((double)image.Width / image.Height) * height) : width;
            }
            ThumbnailImage = image.GetThumbnailImage(Width, Height, delegate() { return false; }, System.IntPtr.Zero);
            image.Dispose();
        }

        public void Draw(Graphics gc, int x, int y)
        {
            gc.DrawImage(ThumbnailImage, x, y, Width, Height);
        }
    }

    public class ThumbnailDecorator : IThumbnail
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public IThumbnail Thumbnail { get; set; }

        public ThumbnailDecorator(IThumbnail thumbnail)
        {
            Thumbnail = thumbnail;
        }

        public virtual void Draw(Graphics gc, int x, int y)
        {
            Thumbnail.Draw(gc, x, y);
        }
    }

    public class FramedPhoto : ThumbnailDecorator
    {
        public Brush FrameBrush { get; set; }
        public Pen BorderPen { get; set; }
        public int BorderWidth { get; set; }

        public FramedPhoto(IThumbnail photo, int frameWidth, int frameHeight) : base(photo)
        {
            Width = frameWidth;
            Height = frameHeight;
            FrameBrush = Brushes.LightGray;
            BorderPen = Pens.Gray;
            BorderWidth = 1;
        }

        public override void Draw(Graphics gc, int x, int y)
        {
            gc.FillRectangle(FrameBrush, x, y, Width, Height);
            gc.DrawRectangle(BorderPen, x, y, Width, Height);
            int dx = (Width - Thumbnail.Width) / 2;
            int dy = (Height - Thumbnail.Height) / 2;
            base.Draw(gc, x + dx, y + dy);
        }
    }

    public class TaggedPhoto : ThumbnailDecorator
    {
        public string Tag { get; set; }

        public TaggedPhoto(IThumbnail photo, string tag) : base(photo)
        {
            Tag = tag;
            Width = photo.Width;
            Height = photo.Height + 20;
        }

        public override void Draw(Graphics gc, int x, int y)
        {
            base.Draw(gc, x, y);
            Font font = new Font("Verdana", 8);
            StringFormat strFrmt = new StringFormat();
            strFrmt.Alignment = StringAlignment.Center;
            strFrmt.LineAlignment = StringAlignment.Center;
            Rectangle r = new Rectangle(x, y + Thumbnail.Height, Thumbnail.Width, 15);
            gc.DrawString(Tag, font, Brushes.Black, r , strFrmt);
         }
    }

    public class ThumbnailBuilder
    {
        public IThumbnail Thumbnail { get; private set; }

        public ThumbnailBuilder()
        {
        }

        public ThumbnailBuilder LoadImage(string path, int width, int height, bool aspect)
        {
            Thumbnail = new ThumbnailComponent(path, width, height, aspect);

            return this;
        }

        public ThumbnailBuilder WithFrame(int w, int h, Brush brush, Pen pen)
        {
            var framedPhoto = new FramedPhoto(Thumbnail, w, h);
            framedPhoto.FrameBrush = brush;
            framedPhoto.BorderPen = pen;
            Thumbnail = framedPhoto;

            return this;
        }

        public ThumbnailBuilder WithTag(string tag)
        {
            var taggedPhoto = new TaggedPhoto(Thumbnail, tag);
            Thumbnail = taggedPhoto;

            return this;
        }
    }
}