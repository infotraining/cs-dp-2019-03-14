﻿using System;
using System.Collections.Generic;
using DP.Behavioral.Chain.Example;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DP.Bahavioral.Chain.Example.Tests
{
    [TestClass]
    public class LoggerTests
    {
        Mock<ILogHandler> mqLogHandler;
        ILogger logger;

        [TestInitialize]
        public void Setup()
        {
            mqLogHandler = new Mock<ILogHandler>();
            mqLogHandler.Setup(mq => mq.CanHandle(It.IsAny<LogEntryType>())).Returns(true);

            logger = new Logger(mqLogHandler.Object);
        }

        [TestMethod]
        public void Logger_DefaultLogging()
        {
            PerformLogging();

            mqLogHandler.Verify(mq => mq.WriteLogMessage(It.IsRegex(@"log msg\d")), Times.Exactly(3));
        }

        private void PerformLogging()
        {
            logger.Log("log msg1", LogEntryType.Warning);
            logger.Log("log msg2", LogEntryType.Error);
            logger.Log("log msg3", LogEntryType.Info);
        }

        [TestMethod]
        public void Logger_DifferentHandlers()
        {
            FakeLogHandler fakeLogHandler = new FakeLogHandler();

            logger.RegisterLogHandler(fakeLogHandler);

            PerformLogging();

            mqLogHandler.Verify(mq => mq.WriteLogMessage("log msg3"), Times.Once);

            fakeLogHandler.Logs.Should().ContainInOrder(new string[] {"log msg1", "log msg2"});
        }

        [TestMethod][ExpectedException(typeof(LogEntryUnhandledException))]
        public void Logger_WhenLogIsUnhandledExceptionIsThrown()
        {
            mqLogHandler.Setup(mq => mq.CanHandle(LogEntryType.Error)).Returns(false);            
            
            PerformLogging();
        }

    }

    public class FakeLogHandler : LogHandler
    {
        public List<string> Logs { get; set; }

        public FakeLogHandler() : base(LogEntryType.Warning | LogEntryType.Error)
        {
            Logs = new List<string>();
        }    

        public override void WriteLogMessage(string msg)
        {
            Logs.Add(msg);
        }
    }
}
