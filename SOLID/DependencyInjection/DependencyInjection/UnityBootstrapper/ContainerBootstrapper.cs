﻿using System.Configuration;
using DependencyInjection.Dependencies;
using Microsoft.Practices.Unity;

namespace DependencyInjection.UnityBootstrapper
{
    public class ContainerBootstrapper
    {
        public static void RegisterTypes(IUnityContainer container)
        {
            var databaseConnectionString
                = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;

            IDataAccessComponent dataAccessComponent = new DataAccessComponent(databaseConnectionString);

            container
                .RegisterType<ILoggingDataSink, LoggingDataSink>()
                .RegisterType<ILoggingComponent, LoggingComponent>(
                    new InjectionConstructor(typeof(ILoggingDataSink), "AppLog"))
                .RegisterInstance(typeof(IDataAccessComponent), dataAccessComponent)
                .RegisterType<IWebServiceProxy>(new InjectionFactory((c) =>
                {
                    string webServiceAddress = ConfigurationManager.AppSettings["MyWebServiceAddress"];

                    return new WebServiceProxy(webServiceAddress);
                }));
        }
    }

    public interface IDataAccessComponentFactory
    {
        IDataAccessComponent Create(object context);
    }

    public interface IWebServiceProxyFactory
    {
        IWebServiceProxy Create(object context);
    }

    public class DataAccessComponentFactory : IDataAccessComponentFactory
    {
        public IDataAccessComponent Create(object context)
        {
            var databaseConnectionString
                = ConfigurationManager.ConnectionStrings["MyConnectionString"].ConnectionString;

            return new DataAccessComponent(databaseConnectionString);
        }
    }

    public class WebServiceProxyFactory : IWebServiceProxyFactory
    {
       public IWebServiceProxy Create(object context)
        {
            string webServiceAddress = ConfigurationManager.AppSettings["MyWebServiceAddress"];

            return new WebServiceProxy(webServiceAddress);
        }
    }

}
