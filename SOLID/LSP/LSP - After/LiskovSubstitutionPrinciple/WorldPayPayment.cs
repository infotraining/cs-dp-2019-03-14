﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiskovSubstitutionPrinciple.Mocks;

namespace LiskovSubstitutionPrinciple
{
    public class WorldPayPayment : PaymentServiceBase
    {
        public WorldPayPayment(string accountId, string accountPassword, string productId)
        {
            AccountId = accountId;
            AccountPassword = accountPassword;
            ProductId = productId;
        }

        public string AccountId { get; set; }
        public string AccountPassword { get; set; }
        public string ProductId { get; set; }

        public override RefundResponse Refund(decimal amount, string transactionId)
        {
            RefundResponse refundResponse = new RefundResponse();

            MockWorldPayWebService worldPayWebService = new MockWorldPayWebService();

            refundResponse.Message = worldPayWebService.MakeRefund(amount, transactionId, AccountId, AccountPassword, ProductId);

            refundResponse.Success = refundResponse.Message.Contains("A_Success");
            
            return refundResponse;
        }
    }
}
