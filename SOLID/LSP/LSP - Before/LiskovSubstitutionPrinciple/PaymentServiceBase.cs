﻿namespace LiskovSubstitutionPrinciple
{
    public abstract class PaymentServiceBase
    {
        public abstract string Refund(decimal amount, string transactionId);
    }
}