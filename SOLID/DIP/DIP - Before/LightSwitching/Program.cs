﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LedDevices;
using System.IO;

namespace LightSwitching
{     

    public class FileLogger
    {
        string _fileName;

        public FileLogger(string fileName)
        {
            _fileName = fileName;
        }

        public void Log(string message)
        {
            using (var logFile = new StreamWriter(_fileName, true))
            {
                logFile.WriteLine(message);
            }
        }
    }

    public class Button
    {
        LedLight _ledLight = new LedLight();
        FileLogger _logger = new FileLogger("log.dat");

        bool _isOn;

        public Button()
        {
            _isOn = false;
        }

        public void Click()
        {
            if (!_isOn)
            {
                _ledLight.SetRGB(255, 255, 255);
            }
            else
            {
                _ledLight.SetRGB(0, 0, 0);
            }

            _isOn = !_isOn;

            _logger.Log("Button was clicked... " + (_isOn ? "ON" : "OFF"));
        }
    }

    class Program
    {       
        static void Main(string[] args)
        {
            Button btn = new Button();

            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
            btn.Click();
        }
    }
}
