﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LedDevices
{
    public class LedLight
    {
        public LedLight()
        {
        }

        public int Red { get; private set; }
        public int Green { get; private set; }
        public int Blue { get; private set; }

        public void SetRGB(int r, int g, int b)
        {
            Red = r;
            Green = g;
            Blue = b;

            Console.WriteLine("LED is set to [{0}, {1}, {2}]", r, g, b);
        }
    }
}
