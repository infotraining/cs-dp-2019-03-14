﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LedDevices;
using System.IO;

namespace LightSwitching
{
    public interface ILogger
    {
        void Log(string message);
    }

    // Composite pattern
    public class LoggingGroup : ILogger
    {
        List<ILogger> _loggers = new List<ILogger>();

        public void Log(string message)
        {
            foreach (var logger in _loggers)
                logger.Log(message);
        }

        public void Add(ILogger logger)
        {
            _loggers.Add(logger);
        }
    }

    public class FileLogger : ILogger
    {
        string _fileName;

        public FileLogger(string fileName)
        {
            _fileName = fileName;
        }

        public void Log(string message)
        {
            using (var logFile = new StreamWriter(_fileName, true))
            {
                logFile.WriteLine(message);
            }
        }
    }

    public class ConsoleLogger : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }

    public interface ISwitch
    {
        void On();

        void Off();

        bool IsOn { get; }
    }

    public class LedSwitch : ISwitch
    {
        LedLight _ledLight;
        static int _counter;

        public LedSwitch(LedLight ledLight, ILogger logger)
        {
            _ledLight = ledLight;
            ++_counter;
            logger.Log(string.Format("LedSwitch#{0} is created...", _counter));
        }

        public bool IsOn { get ; protected set; }

        public void Off()
        {
            _ledLight.SetRGB(0, 0, 0);
            IsOn = false;
        }

        public void On()
        {
            _ledLight.SetRGB(255, 255, 255);
            IsOn = true;
        }
    }


    public class Button
    {
        ISwitch _switch;
        public ILogger Logger { get; set; }        

        public Button(ISwitch s, ILogger l)
        {
            _switch = s;
            Logger = l;
        }

        public void Click()
        {
            if (_switch.IsOn)
            {
                _switch.Off();
            }
            else
            {
                _switch.On();
            }

            Logger.Log("Button was clicked... " + (_switch.IsOn ? "ON" : "OFF"));
        }
    }

    namespace DI
    {
        public class Service
        {
            ILogger _logger;
            int _counter = 0;

            public Service(ILogger logger)
            {
                _logger = logger;
            }

            public void Run()
            {
                ++_counter;

                if (_counter >= 665)
                {
                    _logger.Log("#Error");
                }
            }
        }

        public interface ILoggerCreator
        {
            ILogger Create();
        }

        public class FileLoggerCreator : ILoggerCreator
        {
            public ILogger Create()
            {
                return new FileLogger("errors.log");
            }
        }

        namespace Factory
        {
            public class Service
            {
                ILoggerCreator _loggerCreator;
                int _counter = 0;

                public Service(ILoggerCreator loggerCreator)
                {
                    _loggerCreator = loggerCreator;
                }

                public void Run()
                {
                    ++_counter;

                    if (_counter >= 665)
                    {
                        ILogger logger = _loggerCreator.Create();
                        logger.Log("#Error");
                    }
                }
            }
        }
    }

    class Program
    {       
        static void Main(string[] args)
        {
            var container = new SimpleInjector.Container();            

            // configure the container
            container.Register<ISwitch, LedSwitch>(SimpleInjector.Lifestyle.Singleton);
            container.Register<LedLight>(SimpleInjector.Lifestyle.Singleton);
            //container.Register<ILogger>(() => new FileLogger("log.dat"));
            container.Register<ILogger, ConsoleLogger>(SimpleInjector.Lifestyle.Singleton);

            //Button btn = new Button(new LedSwitch(new LedLight()), new FileLogger("log.dat"));
            Button btn1 = container.GetInstance<Button>();

            btn1.Click();
            btn1.Click();
            btn1.Click();
            btn1.Click();
            btn1.Click();

            Button btn2 = container.GetInstance<Button>();
            //Button btn = new Button(new LedSwitch(new LedLight()), new FileLogger("log.dat"));
            btn2.Click();
            btn2.Click();

            var stdLoggers = new LoggingGroup();
            stdLoggers.Add(new FileLogger("btn2.log"));
            stdLoggers.Add(new FileLogger("btn2-backup.log"));
            stdLoggers.Add(container.GetInstance<ILogger>());

            var extraLoggers = new LoggingGroup();
            extraLoggers.Add(new FileLogger("extra1.log"));
            extraLoggers.Add(new FileLogger("extra2.log"));

            stdLoggers.Add(extraLoggers);

            Console.WriteLine("\nComposite logging:");

            btn2.Logger = stdLoggers;
            btn2.Click();
            btn2.Click();
            btn2.Click();
            btn2.Click();
        }
    }

}
